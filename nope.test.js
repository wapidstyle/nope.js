var test = require("tape");
var nope = require("./");

test("When given input with mapped chars, it should fill them in", function(t) {
  var rawFirstRun = new nope.NopeJsPage();
  rawFirstRun.html[0] = "<!DOCTYPE html>";
  rawFirstRun.html[1] = "<i>\uAAAA</i>";
  var firstRun = rawFirstRun.construct({
    "\uAAAA": "It Works!"
  });

  t.equals(firstRun, "<!DOCTYPE html><i>It Works!</i>",       "Using an object, it should replace \\uAAAA (\uAAAA ) with 'It Works!'");

  var secondRun = rawFirstRun.construct([
    {
      character: "\uAAAA",
      replacement: "It Works!"
    }
  ]);
  t.equals(firstRun, "<!DOCTYPE html><i>It Works!</i>",       "Using an array, it should replace \\uAAAA (\uAAAA ) with 'It Works!'");

  var rawThirdRun = new nope.NopeJsPage();
  rawThirdRun.html[0] = "<!DOCTYPE html>";
  rawThirdRun.html[1] = "<i>\uBBBB</i><b>\uCCCC</b>";
  var thirdRun = rawThirdRun.construct([
    {
      character: "\uBBBB",
      replacement: "Noot Noot!"
    },
    {
      character: "\uCCCC",
      replacement: "We've found Pingu!"
    }
  ]);

  t.equals(thirdRun, "<!DOCTYPE html><i>Noot Noot!</i><b>We've found Pingu!</b>",
                                                              "Using an array with multiple items, it should replace two characters");
  t.end();
});

test("The input should be the input joined", function(t) {
  var firstRun = new nope.NopeJsPage();
  firstRun.html[0] = "Hello ";
  firstRun.html[1] = "World!";
  t.equals(firstRun.construct(), "Hello World!");
  firstRun.html[2] = " I am ";
  firstRun.html[3] = "the be";
  firstRun.html[4] = "st!";
  t.equals(firstRun.construct(), "Hello World! I am the best!");
  t.end();
});
