/*
  nope.js
  version 2.0.0
*/
const argv = module.parent ? {} : require("minimist")(process.argv.slice(2));
const forEach = require("lodash.foreach");

module.exports = {
  NopeJsPage: function(enableConsole) {
    this.html = [];
    this.consoleEnabled = enableConsole || argv.console || false;
    this.construct = function(chars) {
      var output = this.html.join("");
      var log = this.consoleEnabled ? console.error : require("noop3");

      if(chars instanceof Array) {
        chars.forEach(function(char) {
          output = output.replace(char.character, char.replacement);
        });
      } else if(typeof chars === "object") {
        forEach(chars, function(char, index) {
          output = output.replace(index, char);
        });
      }
      return output;
    }
  },
  addons: [],
  version: "1.0.2",
};
