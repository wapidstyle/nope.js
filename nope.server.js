var nope = require("./nope");

console.log("Nope.js | Starting Nope.js server...");
require("http").createServer(function(req, res) {
  var nopeJsPage = new nope.NopeJsPage();
  nopeJsPage.html[0] = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">";
  nopeJsPage.html[1] = "<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">";
  nopeJsPage.html[2] = "<head>";
  nopeJsPage.html[3] = "<title>Hello World!</title>";
  nopeJsPage.html[4] = "</head>";
  nopeJsPage.html[5] = "<body>";
  nopeJsPage.html[6] = "<h1>Hello World!</h1>";
  nopeJsPage.html[7] = "<p>We have recieved your request to " + req.url + "!</p>";
  nopeJsPage.html[8] = "<hr />";
  nopeJsPage.html[9] = "<p><i>Nope.js test server, version 1.0</i>";
  nopeJsPage.html[10] = "<a href=\"http://validator.w3.org/check?uri=referer\"><img";
  nopeJsPage.html[11] = " src=\"http://www.w3.org/Icons/valid-xhtml10\" alt=\"Valid XHTML 1.0 Strict\" height=\"31\"";
  nopeJsPage.html[12] = " width=\"88\" /></a></p>";
  nopeJsPage.html[13] = "</body>";
  nopeJsPage.html[14] = "</html>";
  console.log("REQUEST | Request from " + require("client-ip")(req));
  res.end(nopeJsPage.construct());
}).listen((process.argv[2] || 8080));
console.log("Nope.js | Listening on 127.0.0.1:" + (process.argv[2] || 8080));
