## DEPRECATION NOTICE
> Don't worry - there's a TL;DR and suggestions for upgrading.

This project started on September 28th of 2015. I uploaded c608f98cc, which
was the first commit this project had. Since then, I had uploaded more code,
started the rewrite branch, and much more. However, since the Babel issues
on rewrite, I had begun to slowly let the project die. Looking at my repository
list, it kept falling and falling for 'last updated'.

Finally, I opted to continue it. Revert to 1.0.0, scrap [Gulp](https://bitbucket.org/wapidstyle/nope.js/commits/5ee2503c7c), and basically
scrap the rewrite. I found out that there was no tests or Gulp on master, and
just started rewiring and tacking things on. Finally, I got to 2.0.0. I removed
HTMLHint (as I couldn't get it work and API>CLI) and did a variety of other things,
including reimplementing some of rewrite. In the end, I got a *meh* project.

Since it was a glorified *Array.prototype.join()* statement, I didn't have high
hopes. I finished it for the heck of it.

If you want to hack on it, go for it.

### TL;DR
It was horrible. I made it *meh*.

### Upgrading
If you want a template engine like [cb487b1](https://bitbucket.org/wapidstyle/nope.js/commits/cb487b1),
consider, you know, a darn forEach/replace.

If you want something more elaborate, try:
*   [Handlebars](https://Handlebarsjs.com/). My personal recommendation. It allows
you to use `{{var}}` instead of `\uAD21`. Very little 'logic' in templates.
*   [Funky](https://github.com/mikeal/funky). Saw this when browsing GitHub. Probably
  works well, haven't tried it.
*   [Pug](https://pugjs.org/). Not a fan, but that's preference. It is more of a
  replacement to HTML than a extension.

---

# Nope.js
[![Tests](https://img.shields.io/circleci/project/bitbucket/wapidstyle/nope.js.svg)](https://circleci.com/bb/wapidstyle/nope.js)
[![Linting](https://img.shields.io/shippable/55ee073b1895ca447413882d.svg?label=linting)](https://app.shippable.com/bitbucket/wapidstyle/nope.js)
[![Codecov](https://img.shields.io/codecov/c/bitbucket/wapidstyle/nope.js.svg)](https://codecov.io/bb/wapidstyle/nope.js)  
*Not to be confused with [Node.js](https://nodejs.org/)*  
Nope, Nope.js is not a PHP program.
## What is it?
Nope.js is a simple script that uses JSON to make a webpage dynamically
through Node.js.
> Nope.js was not made because of Node, but because hyper.js already
> exists.

Here's an example:
```javascript
var nopejs = require("nopejs");
var myPage = new nopejs.NopeJsPage();
myPage.html[0] = "<!DOCTYPE html>";
myPage.html[1] = "<head>";
myPage.html[2] = "<title>Hello World</title>";
myPage.html[3] = "<meta charset=\"utf-8\">";
myPage.html[4] = "</head>";
myPage.html[5] = "<body>";
myPage.html[6] = "<p>Hello World</p>";
myPage.html[7] = "</body>";
var constructed = myPage.construct();
```
`constructed` would get the value:
```html
<!DOCTYPE html><head><title>Hello World</title><meta charset="utf-8"></head><body><p>Hello World!</p></body>
```
Beautified, it would get:
```html
<!DOCTYPE html>
<head>
  <title>Hello World</title>
  <meta charset="utf-8">
</head>
<body>
  <p>
    Hello World!
  </p>
</body>
```
